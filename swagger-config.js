module.exports = {
    routePrefix: '/documentation',
    swagger: {
      info: {
        title: 'JIYA API',
        description: 'Backend API for jiya app',
        version: '1.0.0'
      },
      host: 'localhost:5000',
      schemes: ['http'],
      consumes: ['application/json'],
      produces: ['application/json'],
      tags: [
        { name: 'user', description: 'User related end-points' },
      ],
      definitions: {
        SuccessResponse: {
          $id: 'SuccessResponse',
          type: 'object',
          properties: {
            data: { type: 'array' },
            status: { type: 'string' }
          }
        }
      },
      securityDefinitions: {
        apiKey: {
          type: 'apiKey',
          name: 'Authorization',
          in: 'header'
        }
      }
    },
    exposeRoute: true
  }